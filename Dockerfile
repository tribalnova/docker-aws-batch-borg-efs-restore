FROM alpine:latest

ARG BORG_URL='https://github.com/borgbackup/borg/releases/download/1.0.10/borg-linux64'

RUN apk update && apk add nfs-utils ca-certificates wget && update-ca-certificates
RUN wget -q -O /usr/bin/borg $BORG_URL
RUN chmod a+x /usr/bin/borg

ADD borg-restore.sh /var/docker/borg-restore.sh
ENTRYPOINT ["/var/docker/borg-restore.sh"]