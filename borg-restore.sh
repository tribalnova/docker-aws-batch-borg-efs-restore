#!/bin/sh -e

PASSPHRASE=${1:?'Missing passphrase'}
EFS=${2:?'Missing EFS source'}
EFS_BACKUP=${3:?'Missing EFS dest'}
DIRECTORY=$4
RECORD=$5
BORG_REPO=${6:-'borg'}

mkdir -p /mnt/efs
mkdir -p /mnt/efs-backup
umount /mnt/efs || true
umount /mnt/efs-backup || true
mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 $EFS:/ /mnt/efs
mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 $EFS_BACKUP:/ /mnt/efs-backup

REPOSITORIES="/mnt/efs-backup/$BORG_REPO"

if [ -z $DIRECTORY ]; then
    DIRECTORIES=$(find /mnt/efs/* -maxdepth 0 -type d -print)
    for DIR in $DIRECTORIES; do
        NAMES=( ${NAMES[@]} ${DIR##/*/} )
    done
    echo ${NAMES[@]}
elif [ -d "$REPOSITORIES/$DIRECTORY" ]; then
    if [ -z $RECORD ]; then
        sudo BORG_PASSPHRASE=$PASSPHRASE borg list $REPOSITORIES/$DIRECTORY
    else
        sudo BORG_PASSPHRASE=$PASSPHRASE borg info $REPOSITORIES/$DIRECTORY::$RECORD
        echo "Extracting..."
        cd /mnt/efs
        sudo BORG_PASSPHRASE=$PASSPHRASE borg extract $REPOSITORIES/$DIRECTORY::$RECORD
        echo "Moving..."
        if [ -d "/mnt/efs/$DIRECTORY.restore" ]; then
                sudo rm -Rf "/mnt/efs/$DIRECTORY.restore"
        fi
        sudo mv "mnt/efs/$DIRECTORY" "/mnt/efs/$DIRECTORY.restore"
        sudo rmdir mnt/efs
        sudo rmdir mnt
        if [ -d "/mnt/efs/$DIRECTORY.beforeRestore" ]; then
                sudo rm -Rf "/mnt/efs/$DIRECTORY.beforeRestore"
        fi
        sudo mv "mnt/efs/$DIRECTORY" "/mnt/efs/$DIRECTORY.beforeRestore"
        sudo mv "mnt/efs/$DIRECTORY.restore" "/mnt/efs/$DIRECTORY"
        echo "$DIRECTORY ($RECORD) restored, old directory moved to $DIRECTORY.restore"
    fi
fi